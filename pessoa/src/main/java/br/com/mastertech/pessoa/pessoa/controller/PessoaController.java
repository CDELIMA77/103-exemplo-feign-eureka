package br.com.mastertech.pessoa.pessoa.controller;

import br.com.mastertech.pessoa.pessoa.model.Pessoa;
import br.com.mastertech.pessoa.pessoa.model.dto.GetPessoaResponse;
import br.com.mastertech.pessoa.pessoa.model.mapper.PessoaMapper;
import br.com.mastertech.pessoa.pessoa.service.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class PessoaController {

    @Autowired
    private PessoaService pessoaService;

    @Autowired
    private PessoaMapper pessoaMapper;

    @PostMapping
    public Pessoa create(@RequestBody Pessoa pessoa) {
        return pessoaService.create(pessoa);
    }

    @GetMapping("/{id}")
    public GetPessoaResponse getById(@PathVariable Long id) {
        Pessoa byId = pessoaService.getById(id);
        return pessoaMapper.toGetResponse(byId);
    }
}
